package trueskill

import (
	"context"
	"time"

	"google.golang.org/grpc"
)

type Client struct {
	raw  *grpc.ClientConn
	conn TrueSkillClient
	ctx  context.Context
}

func (c *Client) Rate(match Match) (red []*Team, blue []*Team, err error) {
	result, err := c.conn.Rate(c.ctx, &match)
	if err != nil {
		return
	}

	return result.Red, result.Blue, nil
}

func (c *Client) Quality(match Match) (draw float64, red float64, blue float64, err error) {
	result, err := c.conn.Quality(c.ctx, &match)
	if err != nil {
		return
	}

	return result.Draw, result.RedWin, result.BlueWin, nil
}

func (c *Client) Close() error {
	return c.raw.Close()
}

func NewClient(host string) (*Client, error) {
	conn, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}

	ctx, _ := context.WithTimeout(context.Background(), time.Second*10)

	return &Client{
		raw:  conn,
		conn: NewTrueSkillClient(conn),
		ctx:  ctx,
	}, nil
}
