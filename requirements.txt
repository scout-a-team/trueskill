grpcio==1.27.2
grpcio-tools==1.27.2
protobuf==3.11.3
pyhcl==0.4.0
PyYAML==5.3
six==1.14.0
toml==0.10.0
trueskill==0.4.5
