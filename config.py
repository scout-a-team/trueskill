import argparse
import hcl
import json
import os
import sys
import toml
import trueskill
import yaml

import util

PARSERS = {
    "json": json.load,
    "yaml": yaml.load,
    "yml": yaml.load,
    "toml": toml.load,
    "hcl": hcl.load
}


def parse_from_flags():
    """
    Create the flag argument parser for configuring on the command line.

    :return: parsed configuration flags
    """

    # Create the argument parser
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     description="Handle processing of TrueSkill")

    # Add command line flags
    parser.add_argument("--host", type=str, help="The host to listen on")
    parser.add_argument("--port", type=int, help="The port to listen on the host")
    parser.add_argument("--workers", type=int, help="Maximum workers for handling requests")
    parser.add_argument("--backend", type=str, help="Name of backend to use for calculations",
                        choices=["none", "scipy", "mpmath"])
    parser.add_argument("--mu", type=float, help="Initial mean of ratings")
    parser.add_argument("--sigma", type=float,
                        help="Initial standard deviation of ratings. Recommended value is a third of mu")
    parser.add_argument("--beta", type=float, help="Distance which guarantees about 76%% chance of winning. "
                                                   "Recommended value is a half of sigma")
    parser.add_argument("--tau", type=float, help="Dynamic factor which restrains a fixation of ratings. "
                                                  "Recommended value is sigma percent")
    parser.add_argument("--draw_probability", type=float, help="Draw probability between two teams")

    # Parse the flags
    return parser.parse_args()


def read_from_environment():
    """
    Read configuration values from environment variables.

    :return: configuration values
    """
    # Get all values
    host = os.environ.get("TS_HOST")
    port = os.environ.get("TS_PORT")
    workers = os.environ.get("TS_WORKERS")
    backend = os.environ.get("TS_BACKEND")
    mu = os.environ.get("TS_MU")
    sigma = os.environ.get("TS_SIGMA")
    beta = os.environ.get("TS_BETA")
    tau = os.environ.get("TS_TAU")
    draw_probability = os.environ.get("TS_DRAW_PROBABILITY")

    # Mark as non existent if blank
    if host == "":
        host = None
    if backend == "":
        backend = None

    # Try converting to integer
    port = util.convert_to(port, int)
    workers = util.convert_to(workers, int)

    # Try converting to floats
    mu = util.convert_to(mu, float)
    sigma = util.convert_to(sigma, float)
    beta = util.convert_to(beta, float)
    tau = util.convert_to(tau, float)
    draw_probability = util.convert_to(draw_probability, float)

    return host, port, workers, backend, mu, sigma, beta, tau, draw_probability


def determine_config_file():
    """
    Find a configuration file with the name ``config`` in the current directory. It must have one of the
    following extensions: ``json``, ``toml``, ``yaml``, ``yml``, or ``hcl``.

    :return: potential configuration file to use
    """

    # Filter files based on name
    files = [file for file in os.listdir(".") if os.path.isfile(file) and
             (file == "config.json" or file == "config.toml" or file == "config.yaml" or
              file == "config.yml" or file == "config.hcl")]

    # Return the first file, if one exists
    return files[0] if len(files) != 0 else None


def parse_from_file(raw_config: dict):
    """
    Parse the configuration and validate the types from the raw dictionary.

    :param raw_config: raw parsed configuration
    :return: parsed configuration
    """
    host, port, workers, backend, mu, sigma, beta, tau, draw_probability = [None]*9

    if type(raw_config.get("server")) is dict:
        raw_server = raw_config.get("server")  # type: dict

        if type(raw_server.get("host")) is str:
            host = raw_server.get("host")

        if type(raw_server.get("port")) is int:
            port = raw_server.get("port")

        if type(raw_server.get("workers")) is int:
            workers = raw_server.get("workers")

    if type(raw_config.get("trueskill")) is dict:
        raw_ts = raw_config.get("trueskill")  # type: dict

        if type(raw_ts.get("backend")) is str:
            backend = raw_ts.get("backend")

        if type(raw_ts.get("mu")) is float:
            mu = raw_ts.get("mu")

        if type(raw_ts.get("sigma")) is float:
            sigma = raw_ts.get("sigma")

        if type(raw_ts.get("beta")) is float:
            beta = raw_ts.get("beta")

        if type(raw_ts.get("tau")) is float:
            tau = raw_ts.get("tau")

        if type(raw_ts.get("draw_probability")) is float:
            draw_probability = raw_ts.get("draw_probability")

    return host, port, workers, backend, mu, sigma, beta, tau, draw_probability


class Config(object):
    """
    Configure the server using command line flags, environment variables, and a file.
    """

    def __init__(self):
        # Check for configuration from a file
        file = determine_config_file()
        raw_file = {}
        if file is not None:
            # Get the file extension for parsing
            _, ext = os.path.splitext(file)
            raw_file = PARSERS[ext[1:]](open(file))

        # Parse the file configuration
        file_host, file_port, file_workers, file_backend, file_mu, file_sigma, file_beta, \
            file_tau, file_draw_probability = parse_from_file(raw_file)

        # Read from environment
        env_host, env_port, env_workers, env_backend, env_mu, env_sigma, env_beta, env_tau, env_draw_probability\
            = read_from_environment()

        # Read from the command line
        args = parse_from_flags()

        self._host = util.set_config_var(file_host, env_host, args.host, "127.0.0.1")
        self._port = util.set_config_var(file_port, env_port, args.port, 9090)
        self._workers = util.set_config_var(file_workers, env_workers, args.workers, 10)
        self._backend = util.set_config_var(file_backend, env_backend, args.backend, "none")
        self._mu = util.set_config_var(file_mu, env_mu, args.mu, trueskill.MU)
        self._sigma = util.set_config_var(file_sigma, env_sigma, args.sigma, trueskill.SIGMA)
        self._beta = util.set_config_var(file_beta, env_beta, args.beta, trueskill.BETA)
        self._tau = util.set_config_var(file_tau, env_tau, args.tau, trueskill.TAU)
        self._draw_probability = util.set_config_var(
            file_draw_probability, env_draw_probability, args.draw_probability, trueskill.DRAW_PROBABILITY)

        # Validate backend is one of allowed options
        if self._backend != "none" and self._backend != "mpmath" and self._backend != "scipy":
            print("Invalid backend. Must be one of \"none\", \"mpmath\", or \"scipy\"")
            sys.exit(1)
        self._backend = None if self._backend == "none" else self._backend

    @property
    def connection_string(self):
        return f"{self._host}:{self._port}"

    @property
    def workers(self):
        return self._workers

    @property
    def trueskill_params(self):
        return {"mu": self._mu, "sigma": self._sigma,
                "beta": self._beta, "tau": self._tau, "draw_probability": self._draw_probability}
