# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

import trueskill_pb2 as trueskill__pb2


class TrueSkillStub(object):
  """Service for computing the skill of a team from another language
  This is done because Python has the best implementation of Microsoft's TrueSkill
  """

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.Rate = channel.unary_unary(
        '/trueskill.TrueSkill/Rate',
        request_serializer=trueskill__pb2.Match.SerializeToString,
        response_deserializer=trueskill__pb2.Match.FromString,
        )
    self.Quality = channel.unary_unary(
        '/trueskill.TrueSkill/Quality',
        request_serializer=trueskill__pb2.Match.SerializeToString,
        response_deserializer=trueskill__pb2.QualityResult.FromString,
        )


class TrueSkillServicer(object):
  """Service for computing the skill of a team from another language
  This is done because Python has the best implementation of Microsoft's TrueSkill
  """

  def Rate(self, request, context):
    """Recalculate a team's skill after each match
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def Quality(self, request, context):
    """Predict the chance of a draw, red winning and blue winning for a given match
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_TrueSkillServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'Rate': grpc.unary_unary_rpc_method_handler(
          servicer.Rate,
          request_deserializer=trueskill__pb2.Match.FromString,
          response_serializer=trueskill__pb2.Match.SerializeToString,
      ),
      'Quality': grpc.unary_unary_rpc_method_handler(
          servicer.Quality,
          request_deserializer=trueskill__pb2.Match.FromString,
          response_serializer=trueskill__pb2.QualityResult.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'trueskill.TrueSkill', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
