def convert_to(value: str, cls):
    """
    Attempt to convert a value to a specified type

    :param value: string to convert from
    :param cls: class to convert to
    :return: converted value or ``None``
    """
    try:
        return cls(value)
    except (ValueError, TypeError):
        return None


def set_config_var(from_file, from_env, from_flags, default):
    """
    Set a configuration value based on the hierarchy of:
        default => file => env => flags

    :param from_file: value from a configuration file
    :param from_env: value from the environment
    :param from_flags: value from command line flags
    :param default: default value for the option
    :return: value to use as configuration
    """
    if from_flags is not None:
        return from_flags
    elif from_env is not None:
        return from_env
    elif from_file is not None:
        return from_file
    else:
        return default
