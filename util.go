package trueskill

// Get the rating exposure, starting from 0 and converging to the mean
// This should be used for sorting in a leaderboard
func Expose(mu, sigma, defaultMu, defaultSigma float64) float64 {
	k := defaultMu / defaultSigma
	return mu - k*sigma
}
