syntax = "proto3";
package trueskill;

// Service for computing the skill of a team from another language
// This is done because Python has the best implementation of Microsoft's TrueSkill
service TrueSkill {
    // Recalculate a team's skill after each match
    rpc Rate(Match) returns (Match) {}
    // Predict the chance of a draw, red winning and blue winning for a given match
    rpc Quality(Match) returns (QualityResult) {}
}

// Description of who won a match
enum Winner {
    RED = 0;
    BLUE = 1;
    DRAW = 2;
}

// Definition of a completed match
message Match {
    repeated Team red = 1;
    repeated Team blue = 2;

    // This field is only used when passed to Rate
    // When returned from Rate, it will always be DRAW
    // When passed to Quality, it is ignored
    Winner winner = 3;
}

// Representation of a team's skill
message Team {
    double mu = 1;
    double sigma = 2;
}

// Resulting draw chance, red win chance, and blue win chance
message QualityResult {
    double draw = 1;
    double red_win = 2;
    double blue_win = 3;
}
