# TrueSkill
This is a gRPC server and corresponding client that computes the [TrueSkill](https://www.microsoft.com/en-us/research/project/trueskill-ranking-system/) of a team by each match.
A separate server is used to compute the TrueSkill because it is non-trival to implement and Python was the only language I could find it in.
The aptly named [trueskill](https://trueskill.org) is the library we used.

## gRPC
The message format is described in [`trueskill.proto`](./trueskill.proto) and compiled into Python and Golang.
To compile the protobufs, run the following commands:
```shell_script
# Compile for Golang
protoc -I=. --go_out=plugins=grpc:. trueskill.proto

# Compile for Python 3
python3 -m grpc_tools.protoc -I=. --python_out=. --grpc_python_out=. trueskill.proto
```
