from concurrent import futures
import grpc
import math
import sys
import trueskill

from config import Config
import trueskill_pb2
import trueskill_pb2_grpc

# Map the winner of the match to a given rank weighting
# where 0 is the highest rank
WINNER_TO_RANK = {
    trueskill_pb2.Winner.BLUE: [1, 0],
    trueskill_pb2.Winner.RED: [0, 1],
    trueskill_pb2.Winner.DRAW: [0, 0]
}


def team_to_rating(team: trueskill_pb2.Team) -> trueskill.Rating:
    """
    Convert team as defined in the Protocol Buffers to a rating that can be used with TrueSkill.

    :param team: protobuf team to convert
    :return: rating object to be used
    """
    return trueskill.Rating(mu=team.mu, sigma=team.sigma)


def rating_to_team(rating: trueskill.Rating) -> trueskill_pb2.Team:
    """
    Convert a TrueSkill rating to a Protocol Buffer serializable object

    :param rating: rating to convert
    :return: protobuf compatible team
    """
    return trueskill_pb2.Team(mu=round(rating.mu, 4), sigma=round(rating.sigma, 4))


class TrueSkillService(trueskill_pb2_grpc.TrueSkillServicer):
    """
    Calculate the skill of teams based on matches and predict how a given match will turn out. A backend can be
    specified which will use the internal implementation, scipy implementation, or mpmath implementation. The
    valid values for ``backend`` are ``None``, "scipy", and "mpmath".

    :param backend: computational backend to use for TrueSkill
    :param mu: initial mean of ratings
    :param sigma: initial standard deviation of ratings
    :param beta: distance which guarantees about 76% chance of winning
    :param tau: dynamic factor which restrains a fixation of ratings
    :param draw_probability: draw probability between two teams
    """

    def __init__(self, backend: str = None, mu: float = trueskill.MU, sigma: float = trueskill.SIGMA,
                 beta: float = trueskill.BETA, tau: float = trueskill.TAU,
                 draw_probability: float = trueskill.DRAW_PROBABILITY):
        if backend not in trueskill.backends.available_backends():
            print(f"Unavailable backend \"{backend}\", "
                  f"available backends are: {trueskill.backends.available_backends()}")
            sys.exit(1)

        self.env: trueskill.TrueSkill = trueskill.TrueSkill(backend=backend, mu=mu, sigma=sigma,
                                                            beta=beta, tau=tau, draw_probability=draw_probability)

    def Rate(self, request: trueskill_pb2.Match, context) -> trueskill_pb2.Match:
        """
        Compute the change in a team's ability after each match.

        :param request: match to compute for
        :param context: gRPC contextual information
        :return: updated team information
        """

        # Convert protobuf messages to alliances w/ team ratings
        red_alliance = [team_to_rating(team) for team in request.red]
        blue_alliance = [team_to_rating(team) for team in request.blue]

        # Calculate change in team skill
        updated_red, updated_blue = self.env.rate([red_alliance, blue_alliance], ranks=WINNER_TO_RANK[request.winner])

        return trueskill_pb2.Match(
            red=[rating_to_team(rating) for rating in updated_red],
            blue=[rating_to_team(rating) for rating in updated_blue],
            winner=trueskill_pb2.Winner.DRAW
        )

    def Quality(self, request: trueskill_pb2.Match, context) -> trueskill_pb2.QualityResult:
        """
        Predict the chance of a draw, red win, and blue win. The chance of red or blue winning is independent of
        the draw chance, meaning that ``draw_chance + red_win + blue_win != 1.0``.

        :param request: match to predict
        :param context: gRPC contextual information
        :return: predictions for the results of the match
        """

        # Convert protobuf messages to alliances w/ team ratings
        red_alliance = [team_to_rating(team) for team in request.red]
        blue_alliance = [team_to_rating(team) for team in request.blue]

        # Calcualte draw chance
        draw = self.env.quality([red_alliance, blue_alliance])

        # Calculate mu and sigma for entire team
        red_mu = sum(t.mu for t in red_alliance)
        red_sigma = sum((self.env.beta ** 2 + t.sigma ** 2) for t in red_alliance)
        blue_mu = sum(t.mu for t in blue_alliance)
        blue_sigma = sum((self.env.beta ** 2 + t.sigma ** 2) for t in blue_alliance)

        # Calculate win chance of red and blue
        # This is independent of draw chance, meaning: (red_win) + (blue_win) + (draw) != 1.0
        x = (red_mu - blue_mu) / math.sqrt(red_sigma + blue_sigma)
        red_win = self.env.cdf(x)
        blue_win = 1 - red_win

        return trueskill_pb2.QualityResult(
            draw=draw,
            red_win=red_win,
            blue_win=blue_win
        )


# Run the server
if __name__ == "__main__":
    # Load configuration from flags, environment, or file
    cfg = Config()

    # Configure the server
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=cfg.workers))
    trueskill_pb2_grpc.add_TrueSkillServicer_to_server(TrueSkillService(**cfg.trueskill_params), server)
    server.add_insecure_port(cfg.connection_string)

    server.start()
    print(f"Server is running on: {cfg.connection_string}")
    sys.stdout.flush()

    # Ignore keyboard interrupts
    try:
        server.wait_for_termination()
    except KeyboardInterrupt:
        pass
