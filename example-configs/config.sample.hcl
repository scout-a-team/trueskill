# Configuration for the server
server {
    # The host address to listen on
    # Use 0.0.0.0 to listen on all interfaces
    # Default: "127.0.0.1"
    host = "127.0.0.1"
    # The port on the host to listen on
    # Default: 9090
    port = 9090
    # The maximum workers to create for handling requests
    # Default: 10
    workers = 10
}

# Configuration for TrueSkill
trueskill {
    # Name of backend to use for calculations
    # Choices: "none", "scipy", "mpmath"
    # Default: "none"
    backend = "none"
    # Initial mean of ratings
    # Default: 25.0
    mu = 25.0
    # Initial standard deviation of ratings
    # Recommended value is a third of mu
    # Default: 8.333333333333334
    sigma = 8.333333333333334
    # Distance which guarantees about 76% chance of winning
    # Recommended value is half of sigma
    # Default: 4.166666666666667
    beta = 4.166666666666667
    # Dynamic factor which restrains a fixation of ratings
    # Recommend value is sigma percent
    # Default: 0.08333333333333334
    tau = 0.08333333333333334
    # Draw probability between two teams
    # Default: 0.1
    draw_probability = 0.1
}
