FROM python:3.6-slim-stretch

WORKDIR /usr/src/app
EXPOSE 9090

ENV TS_HOST=0.0.0.0
ENV TS_PORT=9090
ENV TS_WORKERS=10
ENV TS_BACKEND=none

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY *.py ./

CMD ["python", "./main.py"]
